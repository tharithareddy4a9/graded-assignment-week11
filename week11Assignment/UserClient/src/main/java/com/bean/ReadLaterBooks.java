package com.bean;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class ReadLaterBooks {
	@EmbeddedId
	private CompositeKey key;
	private String bName;
	private String bGenre;
	private float bPrice;
	private float bRating;


	public ReadLaterBooks() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ReadLaterBooks(CompositeKey key, String bName , String bGenre, 
			float bPrice, float bRating) {
		super();
		this.key = key;
		this.bName = bName;
		this.bGenre = bGenre;
		this.bPrice = bPrice;
		this.bRating = bRating;
	}
	
	public CompositeKey getKey() {
		return key;
	}
	public void setKey(CompositeKey key) {
		this.key = key;
	}

	public String getbName() {
		return bName;
	}


	public void setbName(String bName) {
		this.bName = bName;
	}


	public String getbGenre() {
		return bGenre;
	}


	public void setbGenre(String bGenre) {
		this.bGenre = bGenre;
	}


	public float getbPrice() {
		return bPrice;
	}


	public void setbPrice(float bPrice) {
		this.bPrice = bPrice;
	}


	public float getbRating() {
		return bRating;
	}


	public void setbRating(float bRating) {
		this.bRating = bRating;
	}


	@Override
	public String toString() {
		return "ReadLaterBooks [key=" + key+ ", +bName=" + bName + ", bGenre=" + bGenre + ", bPrice=" + bPrice + ", bRating="
				+ bRating + "]";
	}
	
	
}