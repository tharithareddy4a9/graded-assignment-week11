package com.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Books;
import com.dao.BooksDao;

@Service
public class BooksService {
	@Autowired
	BooksDao booksDao;
	
	public List<Books> getAllBooks() {
		return booksDao.findAll();
	}


	public String storeBook(Books book) {

		if (booksDao.existsById(book.getbId())) {
			return "Book id must be unique";
		} else {
			booksDao.save(book);
			return "Book  stored successfully";
		}
	}

	public String deleteBook(int bookId) {
		if (!booksDao.existsById(bookId)) {
			return "Book details not present";
		} else {
			booksDao.deleteById(bookId);
			return "Book deleted successfully";
		}
	}

	public String updateBookName(Books book) {
		if (!booksDao.existsById(book.getbId())) {
			return "Book details not present";
		} else {
			Books b = booksDao.getById(book.getbId());
			b.setbName(book.getbName());
			booksDao.saveAndFlush(b);
			return "Book updated successfully";
		}
	}

	

	public String updateBookGenre(Books book) {
		if (!booksDao.existsById(book.getbId())) {
			return "Book details not present";
		} else {
			Books b = booksDao.getById(book.getbId());
			b.setbGenre(book.getbGenre());
			booksDao.saveAndFlush(b);
			return "Book updated successfully";
		}
	}
	
	
	public String updateBookPrice(Books book) {
		if (!booksDao.existsById(book.getbId())) {
			return "Book details not present";
		} else {
			Books b = booksDao.getById(book.getbId());
			b.setbPrice(book.getbPrice());
			booksDao.saveAndFlush(b);
			return "Book updated successfully";
		}
	}
	public String updateBookRating(Books book) {
		if (!booksDao.existsById(book.getbId())) {
			return "Book details not present";
		} else {
			Books b = booksDao.getById(book.getbId());
			b.setbRating(book.getbRating());
			booksDao.saveAndFlush(b);
			return "Book updated successfully";
		}
	}
	
}
